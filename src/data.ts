export const data = [
	{
		logo: "icons/shomoji.png",
		name: "Shomoji",
		website: "https://shomoji.vercel.app",
		code: "https://gitlab.com/marianmolina/shomoji",
		readme: "https://gitlab.com/marianmolina/shomoji#shomoji",
		description: "A full stack type-safe and light-weight ecommerce prototype.",
		tags: {
			frontend: [
				["Typescript", "https://www.typescriptlang.org/"],
				["Tailwind", "https://tailwindcss.com/"],
				["React.js", "https://reactjs.org/"],
				["React Router", "https://reactrouter.com/"],
				["Zustand", "https://github.com/pmndrs/zustand"],
				["React Query", "https://react-query-v3.tanstack.com/"],
				["Vite", "https://www.vitejs.dev"],
				["TRPC", "https://trpc.io/"],
			],
			backend: [
				["Node.js", "https://nodejs.org/"],
				["Express.js", "https://expressjs.com/"],
				["Prisma", "https://www.prisma.io/"],
				["MongoDB", "https://www.mongodb.com"],
				["Zod", "https://zod.dev/"],
			],
		},
	},
	{
		logo: "icons/echo_favicon.png",
		name: "Echo",
		website: "https://echoo.vercel.app/login",
		code: "https://gitlab.com/marianmolina/echo",
		readme: "https://gitlab.com/marianmolina/echo#echo",
		description: "A web application to record, visualize and transcribe audios from your browser.",
		tags: {
			frontend: [
				["JavaScript", "https://developer.mozilla.org/en-US/docs/Web/JavaScript"],
				["Tailwind", "https://tailwindcss.com/"],
				["React.js", "https://reactjs.org/"],
				["React Router", "https://reactrouter.com/"],

				["Vite", "https://www.vitejs.dev"],
			],
			backend: [
				["Node.js", "https://nodejs.org/"],
				["Express.js", "https://expressjs.com/"],
				["MongoDB", "https://www.mongodb.com"],
				["JWT", "https://jwt.io/"],
				["Firebase", "https://firebase.google.com/"],
			],
		},
	},
	{
		logo: "icons/mapp_favicon.svg",
		name: "Mapp",
		website: "https://mappp.vercel.app/",
		code: "https://gitlab.com/marianmolina/mapp",
		readme: "https://gitlab.com/marianmolina/mapp#mapp",
		description:
			"Mapp is a simple but addictive geography game. Your goal ? Find all the countries to get the full map colored!",
		tags: {
			frontend: [
				["JavaScript", "https://developer.mozilla.org/en-US/docs/Web/JavaScript"],
				["React.js", "https://reactjs.org/"],
				["D3.js", "https://d3js.org/"],
				["React Router", "https://reactrouter.com/"],
				["Tailwind", "https://tailwindcss.com/"],
				["Vite", "https://www.vitejs.dev"],
			],
			backend: [
				["Node.js", "https://nodejs.org/"],
				["Express.js", "https://expressjs.com/"],
				["MongoDB", "https://www.mongodb.com"],
				["JWT", "https://jwt.io/"],
			],
		},
	},
	{
		logo: "icons/pomotaskus_favicon.png",
		name: "Pomotaskus",
		website: "https://pomotaskus.vercel.app/",
		code: "https://gitlab.com/marianmolina/pomotaskus",
		readme: "https://gitlab.com/marianmolina/pomotaskus#pomotaskus",
		description: "A pomodoro timer synchronised to a list of tasks and other nice features.",
		tags: {
			frontend: [
				["JavaScript", "https://developer.mozilla.org/en-US/docs/Web/JavaScript"],
				["React.js", "https://reactjs.org/"],
				["Tailwind", "https://tailwindcss.com/"],
				["Vite", "https://www.vitejs.dev"],
			],
			backend: [],
		},
	},
]
