// tailwind.config.cjs
module.exports = {
  content: [
    "./public/**/*.html",
    "./src/**/*.{astro,js,jsx,svelte,ts,tsx,vue}",
  ],
  // more options here
  darkMode: "class",
  theme: {
    fontFamily: {
      sans: ["Inter", "sans-serif"],
    },
    extend: {
      colors: {
        color1: "#1a1a1a",
        color2: "#272727",
        color3: "#575757",
        color4: "#c4c4c4",
        color5: "#f3f3f3",
        color6: "#fefefe",

        dark1: "#00020B",
        dark2: "#10121E",
        dark3: "#AEAEAE",
        dark4: "#F3F3F3",
        dark5: "#DBDBDB",

        blue1: "#0A1138",
        blue2: "#222D6D",
        blue3: "#3F4B95",
        blue4: "#7182E8",
        blue5: "#A4AFF2",

        pink1: "#511532",
        pink2: "#8F3862",
        pink3: "#A45B7E",

        purple1: "#3D0D30",
        purple2: "#773E71",
        purple3: "#AE66A7",
      },
    },
  },
};
